#![cfg_attr(feature = "nightly", feature(try_trait))]

/// Matches an expression against a pattern, evaluating to a result
/// expression or an alternative.
///
/// If the pattern matches, the result expression may reference names
/// bound in the pattern.
///
/// This is close to a more general version of `.unwrap_or(...)` that
/// can be used with patterns for any type and that can `return` or
/// `continue` etc. when the pattern does not match.
///
/// # Examples
///
/// An ad-hoc version of `.unwrap_or(...)`:
///
/// ```rust
/// # use std::path::PathBuf;
/// # use try_destructure::match_or;
/// enum Location {
///     Local { path: PathBuf },
///     Remote { addr: String, path: PathBuf },
/// }
///
/// fn get_addr(loc: &Location) -> &str {
///     match_or!(addr, Location::Remote { addr, .. } = loc; "localhost")
/// }
///
/// let loc = Location::Remote {
///     addr: "github.com".into(),
///     path: "/rust-lang/rust".into(),
/// };
/// assert_eq!("github.com", get_addr(&loc));
///
/// let loc = Location::Local { path: "../rust".into() };
/// assert_eq!("localhost", get_addr(&loc));
/// ```
///
/// Skip items with missing properties:
///
/// ```rust
/// # use try_destructure::match_or;
/// # struct Tweet { quoted_tweet: Option<()> }
/// # let tweets = &[];
/// for tweet in tweets {
///     let quoted = match_or!(q, Some(q) = tweet.quoted_tweet; continue);
///     // ... process quote tweets
/// }
/// ```
#[macro_export]
macro_rules! match_or {
    ($result:expr, $pattern:pat = $expression:expr; $alternative:expr) => {
        match $expression {
            $pattern => { $result }
            _ => { $alternative }
        }
    }
}

/// Evaluates to `Some(result)` if the given expression matches the pattern,
/// otherwise  evaluates to `None`.
#[macro_export]
macro_rules! match_opt {
    ($result:expr, $pattern:pat = $expression:expr) => {
        match $expression {
            $pattern => Some($result),
            _ => None,
        }
    }
}

#[cfg(feature = "nightly")]
/// Unwraps an expression or evaluates to the alternative. An unsuccessful
/// value is optionally irrefutably matched against a pattern so that
/// the alternative may reference names bound in the pattern.
///
/// This is analogous to the question mark operator `?`, but with a
/// customizable alternative instead of a fixed `return`.
#[macro_export]
macro_rules! try_or {
    ($expression:expr; $alternative:expr) => {
        match core::ops::Try::into_result($expression) {
            Ok(ok) => ok,
            _ => { $alternative }
        }
    };

    ($expression:expr; $pattern:pat => $alternative:expr) => {
        match core::ops::Try::into_result($expression) {
            Ok(ok) => ok,
            Err(e) => {
                let e = core::convert::From::from(e);
                let $pattern = core::ops::Try::from_error(e);
                $alternative
            }
        }
    }
}


#[cfg(test)]
mod tests {
    use super::match_or;

    macro_rules! get_to_the_end {
        ($($tts:tt)*) => {
            let mut got_to_the_end = false;
            (|| {
                $($tts)*

                got_to_the_end = true;
            })();

            assert!(got_to_the_end, "shouldn't have returned early");
        }
    }

    macro_rules! dont_get_to_the_end {
        ($($tts:tt)*) => {
            let mut got_to_the_end = false;
            (|| {
                $($tts)*

                got_to_the_end = true;
            })();

            assert!(!got_to_the_end, "should have returned early");
        }
    }

    #[derive(Clone, Copy, Debug)]
    enum E {
        A(u32),
        B,
        C { f: u32 },
    }
    use E::*;


    #[test]
    fn fail() {
        dont_get_to_the_end! {
            let v = None;
            let _n = match_or!(n, Some(n) = v; return);
        }

        dont_get_to_the_end! {
            let v = &[1, 2, 3];
            let (_a, _b) = match_or!((a, b), [0, a, b] = v; return);
        }
    }

    #[test]
    fn destructure() {
        get_to_the_end! {
            let v = Some(42);
            let n = match_or!(n, Some(n) = v; return);
            assert_eq!(n, 42);

            let v = A(42);
            assert_eq!(42, match_or!(a, A(a) = v; return));
            let v = B;
            assert_eq!((), match_or!((), B = v; return));
            let v = C{f: 42};
            assert_eq!(42, match_or!(f, C{f} = v; return));
        }

        let mut n = 0;
        for v in &[A(1), B, C {f: 42}, A(10)] {
            n += match_or!(a, A(a) = v; continue);
        }
        assert_eq!(n, 11);

        get_to_the_end! {
            let (a, b, c) = match_or!((a, b, c),
                &[a, b, c] = &(1..=3).into_iter().collect::<Vec<u32>>()[..];
                return);
            assert_eq!((a, b, c), (1, 2, 3));
        }
    }

    #[test]
    fn fallback() {
        let result = Err::<&str, _>("failure");
        assert_eq!("fallback", match_or!(ok, Ok(ok) = result; "fallback"));

        let result = Ok::<_, &str>("success");
        assert_eq!("success", match_or!(ok, Ok(ok) = result; "fallback"));
    }

    #[test]
    fn iter() {
        let mut iter = [1, 2, 3, 4].iter();
        let mut sum = 0;
        loop {
            let item = match_or!(v, Some(v) = iter.next(); break);
            sum += item;
        }
        assert_eq!(1 + 2 + 3 + 4, sum);
    }

    #[test]
    fn dist() {
        let items = [Ok(1), Err(2), Ok(3), Ok(4), Err(5)];
        let mut sum_ok = 0;
        let mut sum_err = 0;
        for item in &items {
            sum_ok += match_or!(v, Ok(v) = item; &0);
            sum_err += match_or!(v, Err(v) = item; &0);
        }
        assert_eq!(1 + 3 + 4, sum_ok);
        assert_eq!(2 + 5, sum_err);
    }

    #[test]
    fn opt() {
        let v = A(42);
        assert_eq!(Some(42), match_opt!(x, A(x) = v));

        let v = B;
        assert_eq!(None, match_opt!(x, A(x) = v));

        let v = C { f: 42 };
        let r = (|| {
            let r = match_opt!(x, A(x) = v)?;
            Some(r)
        })();
        assert_eq!(None, r);
    }

    #[cfg(feature = "nightly")]
    #[test]
    fn trying() {
        let v = Ok::<_, i8>("success");
        let r: Result<_, i16> = loop {
            let r = try_or!(v; e => break e);
            break Ok(r);
        };
        assert_eq!(Ok("success"), r);

        let v = Some("success");
        let r = loop {
            let r = try_or!(v; break None);
            break Some(r);
        };
        assert_eq!(Some("success"), r);

        let v = Err::<&str, i8>(42);
        let r = loop {
            let r = try_or!(v; e => break e);
            break Ok(r);
        };
        assert_eq!(Err(42i16), r);

        let v = None::<u32>;
        let r = loop {
            let r = try_or!(v; break None);
            break Some(r);
        };
        assert_eq!(None, r);
    }
}
